package niaz.testsongs.data;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import niaz.testsongs.R;
import niaz.testsongs.model.Song;
import niaz.testsongs.model.SongsResult;

/**
 * Recycler view adapter
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{
    private Context mContext;
    private Song[] songs;

    /**
     * Constructor
     * @param context
     */
    public MyAdapter(Context context, SongsResult songsResult){
        mContext = context;
        songs = songsResult.getSongs();
    }

    /**
     * View Holder
     */
    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView imageView;
        public TextView textViewTrack;
        public TextView textViewSinger;
        public ViewHolder(View v){
            super(v);
            imageView = (ImageView) v.findViewById(R.id.image_preview);
            textViewTrack = (TextView) v.findViewById(R.id.text_track_name);
            textViewSinger = (TextView) v.findViewById(R.id.text_track_singer);
        }
    }

    /**
     * On create holder
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        // Create a new View
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    /**
     * Bind view holder
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        holder.textViewTrack.setText(songs[position].getTrackName());
        holder.textViewSinger.setText(songs[position].getArtistName());
        Picasso.with(mContext)
                .load(songs[position].getArtworkUrl100())
                .into(holder.imageView);
    }

    /**
     * Get songs count
     * @return
     */
    @Override
    public int getItemCount(){
        return songs.length;
    }

}