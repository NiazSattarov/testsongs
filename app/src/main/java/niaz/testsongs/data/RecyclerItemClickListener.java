package niaz.testsongs.data;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * RecyclerView item click listener
 */
public class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {

    private OnItemClickListener mListener;

    /**
     * On item click listener
     */
    public interface OnItemClickListener {
        public void onItemClick(View view, int i);
    }

    GestureDetector mGestureDetector;

    /**
     * Click listener
     * @param c
     * @param onItemClickListener
     */
    public RecyclerItemClickListener(Context c, OnItemClickListener onItemClickListener) {
        mListener = onItemClickListener;
        mGestureDetector = new GestureDetector(c, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    /**
     * On intercept touch event
     * @param v
     * @param e
     * @return
     */
    @Override
    public boolean onInterceptTouchEvent(RecyclerView v, MotionEvent e) {
        View mChildView = v.findChildViewUnder(e.getX(), e.getY());
        if ((mChildView != null && mListener != null && mGestureDetector.onTouchEvent(e))) {
            mListener.onItemClick(mChildView, v.getChildPosition(mChildView));
            return true;
        }
        return false;
    }

    /**
     * On touch event
     * @param view
     * @param event
     */
    @Override
    public void onTouchEvent(RecyclerView view, MotionEvent event) {
    }

    /**
     * OnRegister
     * @param disallowIntercept
     */
    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}