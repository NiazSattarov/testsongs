package niaz.testsongs.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import niaz.testsongs.R;

/**
 * Main Activity
 */
public class MainActivity extends AppCompatActivity {
    public final static String TAG = "myApp";

    /**
     * OnCreate
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(MainActivity.this, TitleActivity.class);
        startActivity(intent);
        finish();
    }
}
