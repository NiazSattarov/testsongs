package niaz.testsongs.activities;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import java.io.IOException;

import niaz.testsongs.R;
import niaz.testsongs.model.Song;

/**
 * Mediaplayer activity
 * Created by NS on 21.01.2017.
 */
public class PlayerActivity extends AppCompatActivity {
    private final int TIMER_FINISH_INTERVAL = 0xFFFFFFF;
    private final int TIMER_TICK_INTERVAL = 333;
    TextView textViewProgress;
    MediaPlayer player;
    ProgressBar progressBarPlayer;
    CountDownTimer countDownTimer;
    private boolean isPlayerStarted = false;

    String artistName;
    String trackName;
    String imageUrl;
    String trackUrl;

    /**
     * OnCreate
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        Intent intent = getIntent();
        artistName = intent.getStringExtra("artistName");
        trackName = intent.getStringExtra("trackName");
        imageUrl = intent.getStringExtra("imageUrl");
        trackUrl = intent.getStringExtra("trackUrl");

        initInfoViews();
        initPlayerViews();
    }

    /**
     * Init views with song info
     */
    private void initInfoViews() {
        ImageView imageView = (ImageView) findViewById(R.id.image_player_preview);
        Picasso.with( this )
                .load(imageUrl)
                .into(imageView);

        TextView textViewName = (TextView) findViewById(R.id.text_player_name);
        textViewName.setText(trackName);

        TextView textViewSinger = (TextView) findViewById(R.id.text_player_singer);
        textViewSinger.setText(artistName);
    }

    /**
     * Init views for media player
     */
    private void initPlayerViews() {
        textViewProgress = (TextView) findViewById(R.id.text_player_progress);
        progressBarPlayer = (ProgressBar) findViewById(R.id.progressBarPlayer);

        ImageButton buttonPlay = (ImageButton) findViewById(R.id.button_player_play);
        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player == null) {
                    playMusic();
                    isPlayerStarted = true;
                    return;
                }
                if (!player.isPlaying()) {
                    try {
                        player.reset();
                        player.setDataSource(trackUrl);
                        player.prepare();
                        player.start();
                        isPlayerStarted = true;
                    }
                    catch (IOException e) {}
                    textViewProgress.setText("0");
                }
            }
        });

        ImageButton buttonStop = (ImageButton) findViewById(R.id.button_player_stop);
        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player == null) {
                    return;
                }
                player.stop();
                textViewProgress.setText("0");
                progressBarPlayer.setProgress(0);
                isPlayerStarted = false;
                }
        });

        ImageButton buttonPause = (ImageButton) findViewById(R.id.button_player_pause);
        buttonPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player == null || !isPlayerStarted) {
                    return;
                }
                if(player.isPlaying()){
                    player.pause();
                }
                else {
                    player.start();
                }
            }
        });

    }

    /**
     * Manage audio progress
     */
    private void manageProgressBar() {
        final int duration = player.getDuration();

        countDownTimer = new CountDownTimer(TIMER_FINISH_INTERVAL, TIMER_TICK_INTERVAL) {
            @Override
            public void onTick(long l) {
                if (player == null) {
                    return;
                }
                if (!player.isPlaying()){
                    return;
                }
                if (duration != 0) {
                    int position = player.getCurrentPosition();
                    int percent = (position * 100) / duration;
                    textViewProgress.setText(String.valueOf(position / 1000));
                    progressBarPlayer.setProgress(percent);

                    if (position / 1000 == duration / 1000) {
                        textViewProgress.setText("0");
                        progressBarPlayer.setProgress(0);
                        player.stop();
                        isPlayerStarted = false;
                    }
                }
            }

            @Override
            public void onFinish() {
            }
        }.start();
    }

    /**
     * Play music first time
     */
    private void playMusic() {
        try {
            player = new MediaPlayer();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setDataSource(trackUrl);
            player.prepare();
            player.start();
            manageProgressBar();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error to play music", Toast.LENGTH_LONG).show();
            player = null;
        }
    }

    /**
     * OnPause. We should stop timer and sound
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        if (player != null) {
            player.stop();
        }
    }

}
