package niaz.testsongs.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import niaz.testsongs.R;

/**
 * Title Activity
 * Created by NS on 20.01.2017.
 */
public class TitleActivity  extends AppCompatActivity {
    private final Handler handler = new Handler();

    /**
     * on create
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title);

        // Delay timer for 5 seconds
        handler.postDelayed(runnable, 5000);
    }

    /**
     * Runnable for delay
     */
    final Runnable runnable = new Runnable() {
        public void run() {
            startLoginActivity();
        }
    };

    /**
     * Method to run Login activity
     */
    private void startLoginActivity() {
        Intent intent = new Intent(TitleActivity.this, SearchActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * onClick listener
     * @param v
     */
    public void  clickTitle(View v) {
        handler.removeCallbacks(runnable);
        startLoginActivity();
    }
}
