package niaz.testsongs.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.view.Menu;
import android.widget.Toast;

import static niaz.testsongs.activities.MainActivity.TAG;
import niaz.testsongs.R;
import niaz.testsongs.data.MyAdapter;
import niaz.testsongs.data.RecyclerItemClickListener;
import niaz.testsongs.model.Song;
import niaz.testsongs.model.SongsResult;

/**
 * Activity to show list or table of songs
 * Created by NS on 20.01.2017.
 */
public class SongsActivity  extends AppCompatActivity {
    private Context mContext;
    private final int COLUMNS_LIST = 1;
    private final int COLUMNS_PORTRAIT = 2;
    private final int COLUMNS_LANDSCAPE = 3;
    private int columns = COLUMNS_PORTRAIT;
    int columnsNew = COLUMNS_PORTRAIT;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    String searchString;
    SongsResult songsResult;

    /**
     * OnCreate
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_songs);
        Log.d(TAG, "SongActivity.......");


        searchString = getIntent().getStringExtra("search");
        if (searchString == null) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internal_error), Toast.LENGTH_LONG).show();
            return;
        }
        String result = getIntent().getStringExtra("json");
        if (result == null) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internal_error), Toast.LENGTH_LONG).show();
            return;
        }
        songsResult = SearchActivity.convertJsonToSongs(result);
        if (songsResult == null) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internal_error), Toast.LENGTH_LONG).show();
            return;
        }


        columns = COLUMNS_PORTRAIT;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            columns = COLUMNS_LANDSCAPE;
        }
        else {
            columns = COLUMNS_PORTRAIT;
        }
        columnsNew = columns;

        initViews();
        addListenerToRecycleView();
    }

    /**
     * On changing screen orientation
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        columnsNew = COLUMNS_PORTRAIT;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            columnsNew = COLUMNS_LANDSCAPE;
        }
        else {
            columnsNew = COLUMNS_PORTRAIT;
        }

        // Don't init screen in list view mode
        if (columns == COLUMNS_LIST) {
            return;
        }
        if (columns != columnsNew) {
            columns = columnsNew;
            initViews();
            addListenerToRecycleView();
        }
    }

    /**
     * Init views
     */
    private void initViews(){
        TextView textViewTitle = (TextView) findViewById(R.id.text_songs_title);
        textViewTitle.setText(getResources().getString(R.string.songs_names_title) + " \"" + searchString + "\"");

        mContext = getApplicationContext();

        // Get the widgets reference from XML layout
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        mLayoutManager = new GridLayoutManager(mContext, columns);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // Initialize a new instance of RecyclerView Adapter
        mAdapter = new MyAdapter(mContext, songsResult);

        // Set the adapter for RecyclerView
        mRecyclerView.setAdapter(mAdapter);
    }

    /**
     * Listener for RecyclerView
     */
    private void addListenerToRecycleView(){
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(),
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int i) {
                                Song[] songs = songsResult.getSongs();
                                Intent intent = new Intent(SongsActivity.this, PlayerActivity.class);
                                intent.putExtra("artistName", songs[i].getArtistName() );
                                intent.putExtra("trackName", songs[i].getTrackName());
                                intent.putExtra("imageUrl", songs[i].getArtworkUrl100());
                                intent.putExtra("trackUrl", songs[i].getPreviewUrl());
                                startActivity(intent);
                            }
                        })
        );
    }

    /**
     * Options menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Manage options menu
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_list) {
            if (columns != COLUMNS_LIST) {
                columns = COLUMNS_LIST;
                initViews();
                addListenerToRecycleView();
            }
            return true;
        }

        if (id == R.id.action_table) {
            if (columns == COLUMNS_LIST) {
                columns = columnsNew;
                initViews();
                addListenerToRecycleView();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
