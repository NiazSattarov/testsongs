package niaz.testsongs.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import niaz.testsongs.R;
import niaz.testsongs.model.Song;
import niaz.testsongs.model.SongsResult;
import niaz.testsongs.model.ItunesDownload;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static niaz.testsongs.activities.MainActivity.TAG;

/**
 * Search string is entering in the activity
 * Created by NS on 20.01.2017.
 */
public class SearchActivity  extends AppCompatActivity {
    private ProgressBar progressBar;
    private EditText editSearch;
    private Button buttonSearch;
    private String searchString;
    private String searchStringEncoded;

    /**
     * OnCreate
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Log.d(TAG, "SearchAcrivity............");
        initViews();
    }

    /**
     * Init views
     */
    private void initViews(){
        progressBar = (ProgressBar) findViewById(R.id.progress_login);
        editSearch = (EditText) findViewById(R.id.edit_search);
        editSearch.setSingleLine();

        buttonSearch = (Button) findViewById(R.id.button_search);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (progressBar.getVisibility() == View.VISIBLE) {
                    return;
                }
                searchString = editSearch.getText().toString().trim();
                if (searchString.isEmpty()) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.search_string_is_empty), Toast.LENGTH_LONG).show();
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);

                try {
                    searchStringEncoded = URLEncoder.encode(searchString, "UTF-8");
                }
                catch (UnsupportedEncodingException e) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.search_string_error), Toast.LENGTH_LONG).show();
                    return;
                }

                sendRetrofit2Download();

            }
        });
    }


    /**
     * Process respose string
     * @param result
     */
    public void processResponse(String result) {
        progressBar.setVisibility(View.INVISIBLE);
        Log.d (TAG, "response=" + result);

        if (result == null || result.isEmpty()) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            return;
        }

        SongsResult songsResult = convertJsonToSongs(result);
        if (songsResult == null) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.json_error), Toast.LENGTH_LONG).show();
            return;
        }
        if (songsResult.getResultCount() == 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_results), Toast.LENGTH_LONG).show();
            return;
        }

        Intent intent = new Intent(SearchActivity.this, SongsActivity.class);
        intent.putExtra("json", result);
        intent.putExtra("search", searchString);
        startActivity(intent);
    }


    /**
     * Convert json string to objects
     * @param str
     * @return
     */
    public static SongsResult convertJsonToSongs (String str)
    {
        SongsResult songsResult;

        //  Convert JSON to Java object
        ObjectMapper mapper = new ObjectMapper();

        //JSON from String to Object
        try {
            songsResult = mapper.readValue(str, SongsResult.class);
        } catch (IOException e) {
            Log.e(TAG, "*** Error converting Json to obj:" + e);
            return null;
        }
        return songsResult;
    }


    /**
     * Download file from
     * https://itunes.apple.com/search?term=searchString
     */
    private void sendRetrofit2Download() {
        Retrofit retrofit = new Retrofit.Builder().
                baseUrl("https://itunes.apple.com/")
                .build();

        ItunesDownload retrofitDownload = retrofit.create(ItunesDownload.class);
        Call<ResponseBody> call = retrofitDownload.downloadFile("search?term=" + searchStringEncoded);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.INVISIBLE);
                    ResponseBody responseBody = response.body();
                    try {
                        processResponse(responseBody.string());
                    } catch (IOException e) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                        return;
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                return;
            }
        });
    }
}
