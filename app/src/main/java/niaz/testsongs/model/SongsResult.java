package niaz.testsongs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class to store json response from server
 * Created by NS on 20.01.2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SongsResult {
    private static final String	JSON_RESULT_COUNT   = "resultCount";
    private static final String	JSON_SONGS          = "results";

    @JsonIgnore
    private int resultCount;

    @JsonIgnore
    private Song[] songs;

    public SongsResult(){

    }

    // Setters, getters
    @JsonProperty(JSON_SONGS)
    public void setSongs(Song[] songs) {
        this.songs = songs;
    }

    @JsonProperty(JSON_SONGS)
    public Song[] getSongs(){
        return songs;
    }

    @JsonProperty(JSON_RESULT_COUNT)
    public void setResultCount(int resultCount){
        this.resultCount = resultCount;
    }

    @JsonProperty(JSON_RESULT_COUNT)
    public int getResultCount(){
        return resultCount;
    }

}
