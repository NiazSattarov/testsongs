package niaz.testsongs.model;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Interface for request
 * "https://itunes.apple.com/search?term="
 * Created by NS on 25.01.2017.
 */

public interface ItunesDownload {
    @GET
    Call<ResponseBody> downloadFile(@Url String fileUrl);
}
