package niaz.testsongs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class to for song json data
 * Created by NS on 20.01.2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Song {
    private static final String	JSON_KIND               = "kind";
    private static final String	JSON_ARTIST_NAME        = "artistName";
    private static final String	JSON_TRACK_NAME         = "trackName";
    private static final String	JSON_PREVIEW_URL        = "previewUrl";
    private static final String	JSON_ARTWORK_URL_30     = "artworkUrl30";
    private static final String	JSON_ARTWORK_URL_60     = "artworkUrl60";
    private static final String	JSON_ARTWORK_URL_100    = "artworkUrl100";

    @JsonIgnore
    private String kind; //should be "song"

    @JsonIgnore
    private String artistName;

    @JsonIgnore
    private String trackName;

    @JsonIgnore
    private String previewUrl;

    @JsonIgnore
    private String artworkUrl30;

    @JsonIgnore
    private String artworkUrl60;

    @JsonIgnore
    private String artworkUrl100;

    /**
     * Constructor for Jackson
     */
    public Song(){

    }

    // Setters, getters
    @JsonProperty(JSON_KIND)
    public void setKind(String kind){
        this.kind = kind;
    }

    @JsonProperty(JSON_KIND)
    public String getKind(){
        return kind;
    }

    @JsonProperty(JSON_ARTIST_NAME)
    public void setArtistName (String artistName){
        this.artistName = artistName;
    }

    @JsonProperty(JSON_ARTIST_NAME)
    public String getArtistName(){
        return artistName;
    }

    @JsonProperty(JSON_TRACK_NAME)
    public void setTrackName(String trackName){
        this.trackName = trackName;
    }

    @JsonProperty(JSON_TRACK_NAME)
    public String getTrackName(){
        return trackName;
    }

    @JsonProperty(JSON_PREVIEW_URL)
    public void setPreviewUrl(String previewUrl){
        this.previewUrl = previewUrl;
    }

    @JsonProperty(JSON_PREVIEW_URL)
    public String getPreviewUrl(){
        return previewUrl;
    }

    @JsonProperty(JSON_ARTWORK_URL_30)
    public void setArtworkUrl30(String artworkUrl30){
        this.artworkUrl30 = artworkUrl30;
    }

    @JsonProperty(JSON_ARTWORK_URL_30)
    public String getArtworkUrl30(){
        return artworkUrl30;
    }

    @JsonProperty(JSON_ARTWORK_URL_60)
    public void setArtworkUrl60(String artworkUrl60){
        this.artworkUrl60 = artworkUrl60;
    }

    @JsonProperty(JSON_ARTWORK_URL_60)
    public String getArtworkUrl60(){
        return artworkUrl60;
    }

    @JsonProperty(JSON_ARTWORK_URL_100)
    public void setArtworkUrl100(String artworkUrl100){
        this.artworkUrl100 = artworkUrl100;
    }

    @JsonProperty(JSON_ARTWORK_URL_100)
    public String getArtworkUrl100(){
        return artworkUrl100;
    }
}
